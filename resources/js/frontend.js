
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

toggleCommentForm = function (e) {
  $(e.currentTarget).closest('.comment-info').siblings('.comment-body').toggleClass('edit');
};

deleteComment = function (e) {
  let result = confirm('delete comment?');

  let action = $(e.currentTarget).data('action');
  let comment = $(e.currentTarget).closest('.media');
  if (result) {
    $.post(action, {
      _method: 'delete',
    }).done(function (data) {
      comment.remove();
    });
  }
};

$('form.update-comment').submit(function (e) {
  e.preventDefault();

  let comment = $(e.currentTarget).find('[name="comment"]').val();
  let post_id = $(e.currentTarget).find('[name="post_id"]').val();
  let name = $(e.currentTarget).find('[name="name"]').val();

  $.post($(e.currentTarget).attr('action'), {
    _method: 'put',
    post_id: post_id,
    name: name,  
    comment: comment,
  }).done(function (data) {
    $(e.currentTarget).closest('.comment-body').toggleClass('edit');
    $(e.currentTarget).siblings('p').html(comment);
  });
});
