
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

deletePost = function (id) {
  let result = confirm('Do you want to delete the post?');
  if (result) {
    let actionUrl = '/posts/'+id;
    $.post(actionUrl, {_method: 'delete'}).done(function() {
      location.href = '/posts/admin';
    });
  }
};

deleteCategory = function (id) {
  let result = confirm('Do you want to delete the category?');
  if (result) {
    let actionUrl = '/categories/'+id;
    $.post(actionUrl, {_method: 'delete'}).done(function() {
      location.href = '/categories';
    });
  }
};

deleteTag = function (id) {
  let result = confirm('Do you want to delete the tag?');
  if (result) {
    let actionUrl = '/tags/'+id;
    $.post(actionUrl, {_method: 'delete'}).done(function() {
      location.href = '/tags';
    });
  }
};
